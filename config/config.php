<?php

  define('DEBUG', true); // set debug to false for production

  define('DB_NAME', getenv('DB_NAME'));
  define('DB_USER', getenv('DB_USER'));
  define('DB_PASSWORD', getenv('DB_PASSWORD'));
  define('DB_HOST', getenv('DB_HOST')); // database host *** use IP address to avoid DNS lookup

  define('DEFAULT_CONTROLLER', 'Home'); // default controller if there isn't one defined in the url
  define('DEFAULT_LAYOUT', 'default'); // if no layout is set in the controller use this layout.

  define('PROOT', '/ruah/'); // set this to '/' for a live server.
  define('VERSION','0.27'); // release version this can be used to display version or version assets like css and js files useful for fighting cached browser files

  define('SITE_TITLE', 'Ruah MVC Framework'); // This will be used if no site title is set
  define('MENU_BRAND', 'RUAH'); //This is the Brand text in the menu

  define('CURRENT_USER_SESSION_NAME', 'kwXeusqldkiIKjehsLQZJFKJ'); //session name for logged in user
  define('REMEMBER_ME_COOKIE_NAME', 'JAJEI6382LSJVlkdjfh3801jvD'); // cookie name for logged in user remember me
  define('REMEMBER_ME_COOKIE_EXPIRY', 2592000); // time in seconds for remember me cookie to live (30 days)

  define('ACCESS_RESTRICTED', 'Restricted'); //controller name for the restricted redirect
